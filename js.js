
var app = new Vue({
  el: '#app',
  data: {
    LHS: 0,
    RHS: 0,
    operator: "+",
    result: 0
  },
  methods: {
    calculate() {
      switch (this.operator) {
        case "+":
          this.result = (this.LHS - 0) + (this.RHS - 0);
          break;
        case "-":
          this.result = this.LHS - this.RHS;
          break;
        case "*":
          this.result = this.LHS * this.RHS;
          break;
        case "/":
          this.result = this.LHS / this.RHS;
          break;
		default:
			alert('nice operator, man');
		  break;
      }
    }
  }
});